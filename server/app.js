const express = require("express");
const cors = require("cors");
const dotenv = require("dotenv");
const { tenant_Identity } = require("./middleware/auth");
const errorMiddleware = require("./middleware/error");
const app = express();
const axios = require("axios");
const qs = require("querystring");
const createAsyncError = require("./middleware/createAsyncError");
const tenant = require("./routes/tenantRoute");
const { TenantModel } = require("./models");
const ErrorHandler = require("./utils/errorHandler");
const sequence = require("./routes/sequenceRoute");
const user = require("./routes/userRoute");
const open = require("opn");
const { basename } = require("path");
const { supportData } = require("./asserts/supportData");

dotenv.config({ path: "config/config.env" });

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true, limit: "1mb" }));

app.use(tenant_Identity);

app.get("/", (req, res) => res.send("server in working fine"));

app.use("/tenant", tenant);
app.use("/sequence", sequence);
app.use("/user", user);

app.get(
  "/oauth/:email",
  createAsyncError(async (req, res, next) => {
    const email = req.params.email;
    // const support = await TenantModel(req.tenant_id).findOne({ email });

    const support = supportData.find((sup) => sup.email === email);

    if (!support) {
      return next(new ErrorHandler("Support email id doesn't exist", 404));
    }

    const authParams = {
      client_id: support.clientId,
      response_type: "code",
      redirect_uri: support.redirect_uri,
      response_mode: "query",
      scope: "offline_access user.read mail.read",
      state: support.email,
    };

    const { request } = await axios.get(
      `${process.env.AUTHORIZE_URI}/${
        support.tenantId
      }/oauth2/v2.0/authorize?${qs.stringify(authParams)}`
    );

    // console.log(
    //   request.res.responseUrl +
    //     `&login_hint=${encodeURIComponent(support.email)}`
    // );

    open(
      request.res.responseUrl +
        `&login_hint=${encodeURIComponent(support.email)}`
    );

    res.send(
      `Please complete your Authentication <a href=${request.res.responseUrl}>by click this link</a>`
    );
  })
);

app.get(
  `/callback/:name`,
  createAsyncError(async (req, res, next) => {
    console.log("caliing");
    const email = req.query.state;

    // let support = await TenantModel(req.tenant_id).findOne({ email });

    let support = supportData.find((sup) => sup.email === email);

    if (!support) {
      return next(new ErrorHandler("Support email id doesn't exist", 404));
    }

    const authTokenRequest = {
      client_id: support.clientId,
      code: req.query.code,
      redirect_uri: support.redirect_uri,
      client_secret: support.clientSecret,
      scope: "User.Read Mail.Read offline_access",
      grant_type: "authorization_code",
    };

    // console.log(
    //   `${process.env.TOKEN_URI}/${
    //     support.tenantId
    //   }/oauth2/v2.0/token?${qs.stringify(authTokenRequest)}`
    // );

    const { data } = await axios.post(
      `${process.env.TOKEN_URI}/${support.tenantId}/oauth2/v2.0/token`,
      qs.stringify(authTokenRequest),
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    );

    const tenantData = {
      // clientId: support.clientId,
      // tenantId: support.tenantId,
      // clientSecret: support.clientSecret,
      // email: support.email,
      accessToken: data.access_token,
      refreshToken: data.refresh_token,
      expires_in:
        Date.now() + parseInt(`${data.expires_in}`, 10) - 0.002 * 1000,
    };

    supportData.map((sup) =>
      sup.email === email ? { ...sup, ...tenantData } : sup
    );

    // support = await TenantModel(req.tenant_id).findOneAndUpdate(
    //   { email },
    //   tenantData,
    //   {
    //     new: true,
    //     runValidators: true,
    //     useFindAndModify: false,
    //   }
    // );

    const emails = await axios.get(process.env.GRAPH_EMAIL, {
      headers: {
        Authorization: `Bearer ${data.access_token}`,
        Accept: "application/json",
      },
    });

    // await TenantModel(req.tenant_id).deleteOne({ email: support.email });

    // res.status(200).json({ data: emails.data });
    res.send("Authentication successful.");
  })
);

app.all("*", (req, res, next) => {
  next(new ErrorHandler(`${req.originalUrl} Resource Not Found`, 404));
});

app.use(errorMiddleware);

module.exports = app;
