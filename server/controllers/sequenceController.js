const createAsyncError = require("../middleware/createAsyncError");
const { CounterModel } = require("../models");

exports.createNewSequence = createAsyncError(async (req, res, next) => {
  const tenant = await CounterModel(req.tenant_id).create(req.query);

  res.status(201).json({
    success: true,
    message: "Sequence added successfully",
    tenant,
  });
});
