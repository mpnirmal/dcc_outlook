const createAsyncError = require("../middleware/createAsyncError");
const { TenantModel } = require("../models");

exports.createNewTenant = createAsyncError(async (req, res, next) => {
  const tenant = await TenantModel(req.tenant_id).create(req.query);

  res.status(201).json({
    success: true,
    message: "Tenant added successfully",
    tenant,
  });
});

exports.getSingleTenant = createAsyncError(async (req, res, next) => {
  const tenant = await TenantModel(req.tenant_id).findById(req.params.id);

  if (!tenant) {
    return next(new ErrorHandler("Tenant not exist with this email id", 404));
  }

  res.status(201).json({
    success: true,
    tenant,
  });
});

exports.getAllTenants = createAsyncError(async (req, res, next) => {
  const tenants = await TenantModel(req.tenant_id).find();

  res.status(201).json({
    success: true,
    message: "Tenant Lists",
    tenants,
  });
});

exports.deleteTenant = createAsyncError(async (req, res, next) => {
  const tenant = await TenantModel(req.tenant_id).findById(req.params.id);

  if (!tenant) {
    return next(new ErrorHandler("Tenant not found", 404));
  }

  await tenant.deleteOne({ _id: req.params.id });

  res.status(200).json({
    success: true,
    message: "Tenant deleted successfully",
  });
});
