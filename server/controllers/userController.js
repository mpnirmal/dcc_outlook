const createAsyncError = require("../middleware/createAsyncError");
const { UserModel } = require("../models");

exports.createNewUser = createAsyncError(async (req, res, next) => {
  const user = await UserModel.create(req.body);

  res.status(201).json({
    success: true,
    message: "User added successfully",
    user,
  });
});
