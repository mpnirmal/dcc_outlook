const { supportData } = require("../asserts/supportData");
const createAsyncError = require("../middleware/createAsyncError");
const { TenantModel } = require("../models");
const axios = require("axios");

module.exports = createAsyncError(async () => {
  // let support = await TenantModel("db1").find();

  // if (!support) {
  //   return next(
  //     new ErrorHandler("Support id doesn't exist with this email id", 404)
  //   );
  // }

  for (const sup of supportData) {
    if (!sup.accessToken) {
      await axios.get(`http://localhost:5000/oauth/${sup.email}`);
    }
  }
});
