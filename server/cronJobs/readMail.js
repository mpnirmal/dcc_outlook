const { Blob } = require("buffer");
const qs = require("querystring");
const axios = require("axios");
const cheerio = require("cheerio");
const fs = require("fs");
const { join, basename, resolve } = require("path");
const mime = require("mime-types");
// const { ObjectId } = require("mongodb");

const createAsyncError = require("../middleware/createAsyncError");
const { getAccessToken } = require("../utils/getAccessToken");
const { EmailModel } = require("../models");
const {
  handleReplyMessage,
  handleNewMessage,
} = require("../services/emailService");
const { sendMailViaGraphAPI } = require("../utils/sendMailViaGraphAP");

module.exports = createAsyncError(async () => {
  const mutipleEmails = ["nirmal@imperiumapp.com", "samuel@imperiumapp.com"];

  for (const email of mutipleEmails) {
    const tenant = await getAccessToken(email);

    const queryParams = {
      $filter: "isRead eq false",
    };

    const { data } = await axios.get(
      `${process.env.GRAPH_EMAIL}?${qs.stringify(queryParams)}`,
      {
        headers: {
          Authorization: `Bearer ${tenant?.accessToken}`,
          "Content-Type": "application/json",
        },
      }
    );

    // console.log(data);

    if (!data.value) {
      console.log("hi");
      return;
    }

    const unreadedEmails = data.value;

    const previousEmails = await EmailModel("db1")
      .find()
      .select({ message_id: 1 });

    const newEmails = unreadedEmails.filter((mgs) =>
      previousEmails.every((e) => e?.message_id !== mgs?.id)
    );

    const attachmentsArray = [];
    const attachmentsArrayNew = [];
    let ResultData = {};

    for (const mail of newEmails) {
      try {
        const { subject, body, id, hasAttachments } = mail;

        const signature = cheerio.load(body.content, { xmlMode: true });

        signature("#Signature").remove();

        const bodyWithoutSign = signature("body").text();

        ResultData.category = "Network Problems";
        ResultData.sentiment = "Negative";

        const seqId = subject.includes("[#RequestId")
          ? await handleReplyMessage(mail, body.content, ResultData)
          : await handleNewMessage(mail, body.content, ResultData);

        console.log(seqId);

        if (hasAttachments) {
          const attachmentsResponse = await axios.get(
            `${process.env.GRAPH_EMAIL}/${id}/attachments`,
            {
              headers: {
                Authorization: `Bearer ${tenant.accessToken}`,
                Accept: "application/json",
              },
            }
          );

          const attachments = attachmentsResponse?.data.value;

          const attachmentPromises = attachments.map(async (elem) => {
            const extension = elem.name.split(".")[1];
            const buffer = Buffer.from(elem.contentBytes, "base64");

            const localPath = resolve(__dirname, "..", "asserts/");
            const filePath = join(localPath, `file_${Date.now()}.${extension}`);

            if (!fs.existsSync(localPath)) {
              fs.mkdirSync(localPath, { recursive: true });
            }

            fs.writeFileSync(filePath, buffer);

            // const objectId = new ObjectId();
            // const userId = JSON.stringify(objectId);
            // const parsedUserId = JSON.parse(userId);

            let formData = new FormData();
            const buff = fs.readFileSync(filePath);
            const mimeType = mime.lookup(filePath);
            const blob = new Blob([buff], { type: mimeType });

            formData.append("file", blob, basename(filePath));
            formData.append("title", "Developer");
            formData.append("description", "development");
            formData.append("category_id", "664c218dfb5f22a332a8599b");
            formData.append("group_id", "6650e2213785d45408658a0e");

            // formData.append("userID", parsedUserId);
            // formData.append("clientApp", "EMS");
            // formData.append("channel", "Mail");
            // formData.append("sessionId", "1");
            // formData.append("sentBy", "test");

            const resp = await axios.post(process.env.CMS_URI, formData);

            const data = {
              id: elem.id,
              name: elem.name,
              contentType: elem.contentType,
              size: elem.size,
              contentId: elem.contentId,
              contentBytes: elem.contentBytes,
              url: resp.data.knowledge.files,
              extension,
              location: elem.isInline ? "inline" : "attachment",
              filename: elem.filename,
              mailType: "newMail",
              refId: seqId.sequence,
            };

            attachmentsArray.push(data);
          });

          await Promise.all(attachmentPromises);

          let updateAttachment = await EmailModel("db1").findByIdAndUpdate(
            seqId.emailData._id,
            {
              attachments: attachmentsArray,
            },
            { new: true, runValidators: true, useFindAndModify: false }
          );

          // console.log(updateAttachment, "149");
        }

        const textContent = JSON.parse(
          fs.readFileSync(
            join(__dirname, "..", "asserts", "autoAck.json"),
            "utf8"
          )
        );

        const emailTextBody = textContent[subject] || textContent["default"];

        // const sts = await sendMailViaGraphAPI(
        //   mail,
        //   emailTextBody,
        //   seqId.emailData,
        //   email
        // );

        // console.log("finshed", sts);
      } catch (error) {
        console.error("Error processing message:", error);
      }
    }
  }
});
