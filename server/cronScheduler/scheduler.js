const cron = require("node-cron");
const { resolve } = require("path");

module.exports = {
  initCron: (cronjob) => {
    for (const key of Object.keys(cronjob)) {
      if (cron.validate(cronjob[key].frequency)) {
        cron.schedule(cronjob[key].frequency, () => {
          try {
            const handler = require(resolve(cronjob[key].handler));
            handler();
          } catch (err) {
            throw new Error(err.message);
          }
        });
      }
    }
  },
};
