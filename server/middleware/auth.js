const ErrorHandler = require("../utils/errorHandler");
const createAsyncError = require("./createAsyncError");

exports.tenant_Identity = createAsyncError(async (req, res, next) => {
  const tenant_id = req.header("tenant_id") || "db1";

  if (!tenant_id) {
    return next(
      new ErrorHandler("Need tenant_id to access this resource", 401)
    );
  }

  req.tenant_id = tenant_id;

  next();
});
