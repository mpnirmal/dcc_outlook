const mongoose = require("mongoose");

exports.counterSchema = new mongoose.Schema(
  {
    id: {
      type: String,
      required: true,
    },
    sequence: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true }
);
