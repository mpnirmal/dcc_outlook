const mongoose = require("mongoose");

exports.emailSchema = new mongoose.Schema(
  {
    status: {
      type: String,
      required: false,
    },
    subject: {
      type: String,
      required: true,
    },
    fromList: {
      type: String,
    },
    fromName: {
      type: String,
    },
    toList: {
      type: String,
    },
    conversationId: {
      type: String,
    },
    ccList: {
      type: Array,
      required: false,
    },
    bccList: {
      type: Array,
      required: false,
    },
    uid: {
      type: Number,
    },
    emailBody: {
      type: String,
      required: false,
    },
    message_id: {
      type: String,
      required: false,
    },
    receivedTime: {
      type: Date,
    },
    attachments: {
      type: Array,
    },
    requestID: {
      type: Number,
      required: false,
    },
    agentID: {
      type: String,
      required: false,
    },
    agent_id: {
      type: String,
      required: false,
    },
    category: {
      type: String,
      required: false,
    },
    sentiment: {
      type: String,
      required: false,
    },
    ticketStatus: {
      type: Boolean,
      default: false,
    },
    ticketID: {
      type: String,
      required: false,
    },
    ticket_autoID: {
      type: String,
      required: false,
    },
    TenantID: {
      type: String,
      required: false,
    },
    agent_emailid: {
      type: String,
      required: false,
    },
    skillset: {
      type: String,
      required: false,
    },
    agentEndStatus: {
      type: Boolean,
      default: false,
    },
    is_queued: {
      type: Boolean,
      default: false,
    },
    is_approved: {
      type: Boolean,
      default: false,
    },
    supervisorFlag: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);
