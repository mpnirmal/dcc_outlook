const mongoose = require("mongoose");

const { supportSchema } = require("./supportModel");
const { emailSchema } = require("./emailModel");
const { counterSchema } = require("./counterModel");
const UserModel = require("./userModel");

const getModelFromTenantDB = (tenantId, modelName, schema) => {
  const dbName = `tenant_${tenantId}`;
  const tenant_DB = mongoose.connection.useDb(dbName, { useCache: true });

  if (!tenant_DB.models[modelName]) {
    tenant_DB.model(modelName, schema);
  }

  return tenant_DB.models[modelName];
};

module.exports = {
  TenantModel: (t_id) => getModelFromTenantDB(t_id, "Support", supportSchema),
  EmailModel: (t_id) => getModelFromTenantDB(t_id, "Email", emailSchema),
  CounterModel: (t_id) =>
    getModelFromTenantDB(t_id, "TrackingId", counterSchema),
  UserModel,
};
