const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");

const supportSchema = new mongoose.Schema(
  {
    clientId: {
      type: String,
      required: true,
    },
    clientSecret: {
      type: String,
      required: true,
      unique: true,
    },
    tenantId: {
      type: String,
      required: true,
    },
    redirect_uri: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    accessToken: {
      type: String,
      required: false,
    },
    refreshToken: {
      type: String,
      required: false,
    },
    TenantID: {
      type: String,
      required: false,
    },
    skillset: {
      type: String,
      required: false,
    },
    expires_in: {
      type: String,
      required: false,
    },
    isDeleted: {
      type: Boolean,
      default: false,
    },
  },
  { timestamps: true }
);

// tenantSchema.pre("save", async function (next) {
//   if (!this.isModified("clientSecret")) {
//     next();
//   }

//   this.clientSecret = await bcrypt.hash(this.clientSecret, 10);
// });

module.exports = { supportSchema };
