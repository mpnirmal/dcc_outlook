const express = require("express");
const { createNewSequence } = require("../controllers/sequenceController");

const router = express.Router();

router.route("/new").post(createNewSequence);

module.exports = router;
