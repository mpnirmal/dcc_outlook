const express = require("express");
const {
  createNewTenant,
  getSingleTenant,
  deleteTenant,
  getAllTenants,
} = require("../controllers/tenantController");

const router = express.Router();

router.route("/new").post(createNewTenant).get(getAllTenants);
router.route("/:id").delete(deleteTenant).get(getSingleTenant);

module.exports = router;
