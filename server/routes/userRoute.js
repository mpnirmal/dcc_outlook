const express = require("express");
const { createNewUser } = require("../controllers/userController");

const router = express.Router();

router.route("/new").post(createNewUser);

module.exports = router;
