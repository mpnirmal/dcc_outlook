const app = require("./app");
const dbconnection = require("./config/database");
const cronJob = require("./utils/cronJob");
const scheduler = require("./cronScheduler/scheduler");

process.on("uncaughtException", (err) => {
  console.log(`Error: ${err.message}`);
  console.log("Shutting down the server due to Uncaught Exception");

  process.exit(1);
});

dbconnection();

scheduler.initCron(cronJob);

const server = app.listen(process.env.PORT, () => {
  console.log(`server is working on http://localhost:${process.env.PORT}`);
});

process.on("unhandledRejection", (err) => {
  console.log(`Error: ${err.message}`);
  console.log("Shutting down the server due to Unhandled Promise Rejection");

  server.close(() => process.exit(1));
});
