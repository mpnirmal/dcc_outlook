const { CounterModel, EmailModel } = require("../models");

exports.handleReplyMessage = async (mail, bodyContent, ResultData) => {
  const replySubject = mail.subject.replace(/^Re:\s*/i, "").trim();

  const emailNew = await EmailModel("db1")
    .findOne({
      fromList: mail.sender.emailAddress.address,
      subject: replySubject,
      status: "New",
      ticketStatus: false,
      agentID: { $ne: "NULL" },
    })
    .sort({ createdAt: -1 });

  const { sequence } = await CounterModel("db1").findOne({ id: "autoId" });

  const emailData = await EmailModel("db1").create({
    subject: replySubject,
    fromList: mail.sender.emailAddress.address,
    fromName: mail.sender.emailAddress.name,
    toList: mail.toRecipients[0].emailAddress.address,
    ccList: mail.ccRecipients,
    bccList: mail.bccRecipients,
    message_id: mail.id,
    emailBody: bodyContent,
    receivedTime: mail.receivedDateTime,
    conversationId: mail.conversationId,
    folderName: "Inbox",
    agentID: emailNew.agentID || "NULL",
    ticketID: emailNew.ticketID || "",
    ticket_autoID: emailNew.ticket_autoID || "",
    status: "CustReplied",
    category: ResultData.category,
    sentiment: ResultData.sentiment,
    skillset: emailNew.skillset || "",
    agent_id: emailNew.agent_id || "",
    agent_emailid: emailNew.agent_emailid || "",
  });

  // console.log(emailData);

  return { sequence, emailData };
};

exports.handleNewMessage = async (mail, bodyContent, ResultData) => {
  let { sequence } = await CounterModel("db1").findOneAndUpdate(
    { id: "autoId" },
    { $inc: { sequence: 1 } },
    { new: true, runValidators: true, useFindAndModify: false }
  );

  const emailData = await EmailModel("db1").create({
    status: "New",
    subject: mail.subject
      ? `[RequestId-${sequence}] ${mail.subject}`
      : `[RequestId-${sequence}] No Subject`,
    fromList: mail.sender.emailAddress.address,
    fromName: mail.sender.emailAddress.name,
    toList: mail.toRecipients[0]?.emailAddress.address,
    ccList: mail.ccRecipients,
    bccList: mail.bccRecipients,
    emailBody: bodyContent,
    requestID: sequence,
    message_id: mail.id,
    attachments: [],
    folderName: "Inbox",
    agentID: "NULL",
    category: ResultData.category,
    sentiment: ResultData.sentiment,
    skillset: "Developer",
    receivedTime: mail.receivedDateTime,
    conversationId: mail.conversationId,
    agent_emailid: "",
  });

  // console.log(emailData);

  return { sequence, emailData };
};

exports.fetchSkillsetFromEmail = async (subject) => {
  try {
    const data = JSON.stringify({ type: "skill" });

    const config = {
      method: "post",
      maxBodyLength: Infinity,
      url: process.env.EMAIL_KEYWORD_URL,
      headers: {
        tenantid: TenantID,
        "Content-Type": "application/json",
      },
      data: data,
    };

    const emailKeywordApiResponse = await axios.request(config);
    const dataList = emailKeywordApiResponse.data?.dataList || [];

    if (dataList.length === 0) return null;

    const keywordsArray = dataList.flatMap((item) => item.keywordNames);

    const extractKeywordsWithOccurrence = (text, keywords) => {
      return keywords.reduce((extractedKeywords, keyword) => {
        const regex = new RegExp(`\\b${keyword}\\b`, "gi");
        const matches = text.match(regex);
        extractedKeywords[keyword.toLowerCase()] = matches ? matches.length : 0;
        return extractedKeywords;
      }, {});
    };

    const extractedKeywordsWithOccurrence = extractKeywordsWithOccurrence(
      subject,
      keywordsArray
    );

    let maxCount = 0;
    let skillset = null;
    console.log("Keywords from content:", extractedKeywordsWithOccurrence);

    for (const [keyword, count] of Object.entries(
      extractedKeywordsWithOccurrence
    )) {
      if (count > maxCount) {
        maxCount = count;
        for (const item of dataList) {
          if (item.keywordNames.some((kw) => kw.toLowerCase() === keyword)) {
            skillset = item.skillName;
            break;
          }
        }
      }
    }

    return skillset;
  } catch (error) {
    console.error("Error occurred while fetching data:", error.message);
    throw error;
  }
};
