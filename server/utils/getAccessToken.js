const qs = require("querystring");
const axios = require("axios");

const { TenantModel } = require("../models");
const createAsyncError = require("../middleware/createAsyncError");

exports.getAccessToken = async (email) => {
  let tenant = await TenantModel("db1").findOne({ email });

  if (!tenant) {
    throw new Error("Tenant doesn't exist with this email");
  }

  if (Date.now() <= tenant.expires_in) return tenant;

  const newTokenRequest = {
    client_id: tenant.clientId,
    refresh_token: tenant.refreshToken,
    client_secret: tenant.clientSecret,
    scope: "User.Read Mail.Read offline_access",
    grant_type: "refresh_token",
  };

  try {
    const { data } = await axios.post(
      `${process.env.TOKEN_URI}/${tenant.tenantId}/oauth2/v2.0/token`,
      qs.stringify(newTokenRequest),
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      }
    );

    if (!data.access_token || !data.refresh_token || !data.expires_in) {
      throw new Error("Invalid response from token endpoint");
    }

    const tenantData = {
      accessToken: data.access_token,
      refreshToken: data.refresh_token,
      expires_in:
        Date.now() + parseInt(`${data.expires_in}`, 10) - 0.002 * 1000,
    };

    tenant = await TenantModel("db1").findOneAndUpdate({ email }, tenantData, {
      new: true,
      runValidators: true,
      useFindAndModify: false,
    });

    return tenant;
  } catch (error) {
    console.error("Error refreshing token:", error.message);
    throw new Error("Failed to refresh access token");
  }
};
