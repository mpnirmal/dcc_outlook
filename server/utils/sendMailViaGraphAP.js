const axios = require("axios");
const { getAccessToken } = require("./getAccessToken");
const { TenantModel } = require("../models");

exports.sendMailViaGraphAPI = async (mail, bodyText, emailData, email) => {
  try {
    getAccessToken(email);

    const tenant = await TenantModel("db1").findOne({ email });

    const ccArray = Array.isArray(mail.ccRecipients) ? mail.ccRecipients : [];
    const bccArray = Array.isArray(mail.bccRecipients)
      ? mail.bccRecipients
      : [];

    const data = {
      message: {
        subject: emailData.subject,
        body: {
          contentType: "Text",
          content: bodyText,
        },
        toRecipients: [
          {
            emailAddress: {
              address: mail.sender.emailAddress.address,
            },
          },
        ],
        ccRecipients: ccArray.map((cc) => ({
          emailAddress: {
            name: cc.emailAddress.name,
            address: cc.emailAddress.address,
          },
        })),
        bccRecipients: bccArray.map((bcc) => ({
          emailAddress: {
            name: bcc.emailAddress.name,
            address: bcc.emailAddress.address,
          },
        })),
      },
      saveToSentItems: false,
    };

    const response = await axios.post(
      process.env.SEND_EMAIL_URI,
      JSON.stringify(data),
      {
        headers: {
          "Content-Type": "application/json",
          "Content-Length": Buffer.byteLength(JSON.stringify(data)),
          Authorization: `Bearer ${tenant.accessToken}`,
        },
      }
    );

    console.log("Email sent successfully", JSON.stringify(response.data));

    return response?.data;
  } catch (error) {
    console.error(
      "Error sending email:",
      error.response ? error.response.data : error.message
    );
    return false;
  }
};
